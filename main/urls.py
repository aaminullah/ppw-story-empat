from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('ceritasatu', views.satu, name='storysatu'),
    path('ceritatiga', views.tiga, name='storytiga'),
    path('ceritalima', views.lima, name='storylima'),
    path('simpan', views.simpan),
]
