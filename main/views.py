from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, response
from .forms import Input_Form


def home(request):
    return render(request, 'main/home.html')

def satu(request):
    return render(request, 'main/ceritasatu.html')

def tiga(request):
    return render(request, 'main/ceritatiga.html')

def lima(request):
    response = {
        'input_form' : 'Input_Form'
        }
    return render(request, 'main/ceritalima.html', response)

def simpan(request):
    form = Input_Form(request.POST)
    if (form.is_valid() and request.method == "POST"):
        form.save()
    return HttpResponseRedirect('/ceritalima')