from django.db import models

# Create your models here.

class MatKul(models.Model) :
    namaMatkul = models.Field(max_length=90)
    kelasMatkul = models.CharField(max_length=5)
    namaDosen = models.CharField(max_length=60)
    jamKuliah = models.CharField(max_length=30)


    def __str__(self):
        return self.namaMatkul

