from django import forms
from .models import MatKul

class Input_Form(forms.ModelForm):
    class Meta:
        model = MatKul
        fields = ['matkul']

    error_messages = {
        'required' : 'Please Type'
    }

    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Nama Mata Kuliah'
    }

    matkul = forms.Field(required=True, widget=forms.TextInput(attrs=input_attrs))
